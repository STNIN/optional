#include <iostream>
#include <cassert>

#include "../../include/optional/optional.hpp"

void ifPresentPrimitivePointerConsumerNull(){
    try{
        Optional<int *>::of(new int(10)).ifPresent(nullptr);
        assert(false);
    }catch (const std::invalid_argument &exception){
        assert(true);
        std::cout << "[TEST]::[ifPresentPrimitivePointerConsumerNull]::SUCCESS::" << exception.what() << std::endl;
    };
}

void ifPresentPrimitivePointerConsumer(){
    int sum = 10;
    Optional<int *>::of(new int(10)).ifPresent([&sum](int const *const value) { sum += *value; });
    assert(sum == 20);
    std::cout << "[TEST]::[ifPresentPrimitivePointerConsumer]::SUCCESS" << std::endl;
}

void ifPresentAbstractPointerConsumerNull(){
    class Test{ public: int const x = 10; };
    try{
        Optional<Test *>::of(new Test()).ifPresent(nullptr);
        assert(false);
    }catch (const std::invalid_argument &exception){
        assert(true);
        std::cout << "[TEST]::[ifPresentAbstractPointerConsumerNull]::SUCCESS::" << exception.what() << std::endl;
    };
}

void ifPresentAbstractPointerConsumer(){
    class Test{ public: int x = 10; };
    Test test = Test();
    Optional<Test *>::of(new Test()).ifPresent([&test](Test const *const value) { test.x += value->x; });
    assert(test.x == 20);
    std::cout << "[TEST]::[ifPresentAbstractPointerConsumer]::SUCCESS" << std::endl;
}

void ifNotPresent(){
    class Test{ public: int x = 10; };
    int sum = 10;
    Optional<int *>::ofNullable(nullptr).ifPresent([&sum](int const *const value) { sum += *value; });
    Optional<int *>::empty().ifPresent([&sum](int const *const value) { sum += *value; });
    Test test = Test();
    Optional<Test *>::ofNullable(nullptr).ifPresent([&test](Test const *const value) { test.x += value->x; });
    Optional<Test *>::empty().ifPresent([&test](Test const *const value) { test.x += value->x; });
    assert(sum == 10);
    assert(test.x = 10);
    std::cout << "[TEST]::[ifNotPresent]::SUCCESS" << std::endl;
}