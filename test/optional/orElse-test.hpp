#include <iostream>
#include <cassert>

#include "../../include/optional/optional.hpp"

void orElsePrimitiveValue(){
    assert(Optional<int>::of(10).orElse(20) == 20);
    assert(Optional<float>::of(2.5).orElse(5.5) == 5.5);
    assert(Optional<char>::of('a').orElse('b') == 'b');
    assert(Optional<double>::of(5.5).orElse(10.5) == 10.5);
    assert(Optional<std::string>::of("ola").orElse("oi") == "oi");
    assert(Optional<bool>::of(false).orElse(true));
    std::cout << "[TEST]::[orElsePrimitiveValue]::SUCCESS" << std::endl;
}

void orElsePrimitiveValuePointerReturningPointerNull(){
    try{
        Optional<int *>::empty().orElse(nullptr);
        assert(false);
    }catch (const std::invalid_argument &exception){
        assert(true);
        std::cout << "[TEST]::[orElsePrimitiveValuePointerReturningPointerNull]::SUCCESS::" << exception.what() << std::endl;
    };
}

void orElsePrimitiveValuePointerReturningPrimitive(){
    assert(Optional<int *>::of(new int(10)).orElse(20) == 10);
    assert(Optional<float *>::of(new float(2.5)).orElse(5.5) == 2.5);
    assert(Optional<char *>::of(new char('a')).orElse('b') == 'a');
    assert(Optional<std::string *>::of(new std::string("ola")).orElse("oi") == "ola");
    assert(Optional<bool *>::ofNullable(nullptr).orElse(true));
    assert(Optional<double *>::empty().orElse(10.5) == 10.5);
    std::cout << "[TEST]::[orElsePrimitiveValuePointerReturningPrimitive]::SUCCESS" << std::endl;
}

void orElsePrimitiveValuePointerReturningPointer(){
    int const *const valor = new int(20);
    assert(*Optional<int *>::of(new int(10)).orElse(valor) == 10);
    assert(*Optional<int *>::ofNullable(nullptr).orElse(valor) == 20);
    assert(*Optional<int *>::empty().orElse(valor) == 20);
    delete valor;
    std::cout << "[TEST]::[orElsePrimitiveValuePointerReturningPointer]::SUCCESS" << std::endl;
}

void orElseAbstractValuePointerReturningPointerNull(){
    class Test{ public: int const x = 10; };
    try{
        Optional<Test *>::empty().orElse(nullptr);
        assert(false);
    }catch (const std::invalid_argument &exception){
        assert(true);
        std::cout << "[TEST]::[orElseAbstractValuePointerReturningPointerNull]::SUCCESS::" << exception.what() << std::endl;
    };
}

void orElseAbstractValuePointerReturningAbstract(){
    class Test{
        private:
            int const x;
        public:
            explicit Test(int const x) : x(x) {}
            int const get() const { return x; }
    };
    assert(Optional<Test *>::of(new Test(10)).orElse(Test(20)).get() == 10);
    assert(Optional<Test *>::ofNullable(nullptr).orElse(Test(20)).get() == 20);
    assert(Optional<Test *>::empty().orElse(Test(20)).get() == 20);
    std::cout << "[TEST]::[orElseAbstractValuePointerReturningAbstract]::SUCCESS" << std::endl;
}

void orElseAbstractValuePointerReturningPointer(){
    class Test{
        private:
            int const x;
        public:
            explicit Test(int const x) : x(x) {}
            int const get() const { return x; }
    };
    Test const *const valor = new Test(20);
    assert(Optional<Test *>::of(new Test(10)).orElse(valor)->get() == 10);
    assert(Optional<Test *>::ofNullable(nullptr).orElse(valor)->get() == 20);
    assert(Optional<Test *>::empty().orElse(valor)->get() == 20);
    delete valor;
    std::cout << "[TEST]::[orElseAbstractValuePointerReturningPointer]::SUCCESS" << std::endl;
}