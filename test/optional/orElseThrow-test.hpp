#include <iostream>
#include <cassert>

#include "../../include/optional/optional.hpp"

void orElseThrowPrimitiveProviderNull(){
	try{
		Optional<int *>::of(new int(10)).getOrElseThrow<nullptr_value>(nullptr);
		assert(false);
	}catch (const std::invalid_argument &exception){
		assert(true);
		std::cout << "[TEST]::[orElseThrowPrimitiveProviderNull]::SUCCESS::" << exception.what() << std::endl;
	};
}

void getPointerOrElseThrowPrimitiveProviderNull(){
	try{
		Optional<int *>::of(new int(10)).getPointerOrElseThrow<nullptr_value>(nullptr);
		assert(false);
	}catch (const std::invalid_argument &exception){
		assert(true);
		std::cout << "[TEST]::[getPointerOrElseThrowPrimitiveProviderNull]::SUCCESS::" << exception.what() << std::endl;
	};
}

void orElseThrowAbstractProviderNull(){
	class Test{ public: int const x = 10; };
	try{
		Optional<Test *>::of(new Test()).getOrElseThrow<nullptr_value>(nullptr);
		assert(false);
	}catch (const std::invalid_argument &exception){
		assert(true);
		std::cout << "[TEST]::[orElseThrowAbstractProviderNull]::SUCCESS::" << exception.what() << std::endl;
	};
}

void getPointerOrElseThrowAbstractProviderNull(){
	class Test{ public: int const x = 10; };
	try{
		Optional<Test *>::of(new Test()).getPointerOrElseThrow<nullptr_value>(nullptr);
		assert(false);
	}catch (const std::invalid_argument &exception){
		assert(true);
		std::cout << "[TEST]::[getPointerOrElseThrowAbstractProviderNull]::SUCCESS::" << exception.what() << std::endl;
	};
}

void orElseThrowPrimitiveThrow(){
	try{
		Optional<int *>::ofNullable(nullptr).getOrElseThrow<nullptr_value>([]() { return nullptr_value("Exception threw"); });
		assert(false);
	}catch (const nullptr_value &exception){
		assert(true);
		std::cout << "[TEST]::[orElseThrowPrimitiveThrow]::SUCCESS::" << exception.what() << std::endl;
	};
}

void getPointerOrElseThrowPrimitiveThrow(){
	try{
		Optional<int *>::empty().getPointerOrElseThrow<nullptr_value>([]() { return nullptr_value("Exception threw"); });
		assert(false);
	}catch (const nullptr_value &exception){
		assert(true);
		std::cout << "[TEST]::[getPointerOrElseThrowPrimitiveThrow]::SUCCESS::" << exception.what() << std::endl;
	};
}

void orElseThrowAbstractThrow(){
	class Test{ public: int const x = 10; };
	try{
		Optional<Test *>::ofNullable(nullptr).getOrElseThrow<nullptr_value>([]() { return nullptr_value("Exception threw"); });
		assert(false);
	}catch (const nullptr_value &exception){
		assert(true);
		std::cout << "[TEST]::[orElseThrowAbstractThrow]::SUCCESS::" << exception.what() << std::endl;
	};
}

void getPointerOrElseThrowAbstractThrow(){
	class Test{ public: int const x = 10; };
	try{
		Optional<Test *>::empty().getPointerOrElseThrow<nullptr_value>([]() { return nullptr_value("Exception threw"); });
		assert(false);
	}catch (const nullptr_value &exception){
		assert(true);
		std::cout << "[TEST]::[getPointerOrElseThrowAbstractThrow]::SUCCESS::" << exception.what() << std::endl;
	};
}

void orElseThrowPrimitiveGet(){
	assert(Optional<int *>::ofNullable(new int(10)).getOrElseThrow<nullptr_value>([]() { return nullptr_value("Exception threw"); }) == 10);
	assert(*Optional<int *>::ofNullable(new int(10)).getPointerOrElseThrow<nullptr_value>([]() { return nullptr_value("Exception threw"); }) == 10);
	std::cout << "[TEST]::[orElseThrowPrimitiveGet]::SUCCESS" << std::endl;
}

void orElseThrowAbstractGet(){
	class Test{
		private:
			int const x;
		public:
			explicit Test(int const x) : x(x) {}
			int const get() const { return x; }
	};
	assert(Optional<Test *>::of(new Test(10)).getOrElseThrow<nullptr_value>([]() { return nullptr_value("Exception threw"); }).get() == 10);
	assert(Optional<Test *>::of(new Test(10)).getPointerOrElseThrow<nullptr_value>([]() { return nullptr_value("Exception threw"); })->get() == 10);
	std::cout << "[TEST]::[orElseThrowAbstractGet]::SUCCESS" << std::endl;
}