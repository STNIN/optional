#include <iostream>
#include <cassert>

#include "../../include/optional/optional.hpp"

void filterPrimitivePointerPredicateNull(){
    try{
        Optional<int *>::of(new int(10)).filter(nullptr);
        assert(false);
    }catch (const std::invalid_argument &exception){
        assert(true);
        std::cout << "[TEST]::[filterPrimitivePointerPredicateNull]::SUCCESS::" << exception.what() << std::endl;
    };
}

void filterAbstractPointerPredicateNull(){
    class Test{ public: int const x = 10; };
    try{
        Optional<Test *>::of(new Test()).filter(nullptr);
        assert(false);
    }catch (const std::invalid_argument &exception){
        assert(true);
        std::cout << "[TEST]::[filterAbstractPointerPredicateNull]::SUCCESS::" << exception.what() << std::endl;
    };
}

void filterPrimitivePointerIsPresentTrueAndPredicateTrue(){
    assert(Optional<int *>::of(new int(10)).filter([](int const *const value) { return *value == 10; }).get() == 10);
    std::cout << "[TEST]::[filterPrimitivePointerIsPresentTrueAndPredicateTrue]::SUCCESS" << std::endl;
}

void filterPrimitivePointerIsPresentFalseAndPredicateFalse(){
    assert(Optional<int *>::ofNullable(nullptr).filter([](int const *const value) { return *value == 10; }).isEmpty());
    assert(Optional<int *>::empty().filter([](int const *const value) { return *value == 10; }).isEmpty());
    std::cout << "[TEST]::[filterPrimitivePointerIsPresentFalseAndPredicateFalse]::SUCCESS" << std::endl;
}

void filterPrimitivePointerIsPresentTrueAndPredicateFalse(){
    assert(Optional<int *>::of(new int(10)).filter([](int const *const value) { return *value == 20; }).isEmpty());
    std::cout << "[TEST]::[filterPrimitivePointerIsPresentTrueAndPredicateFalse]::SUCCESS" << std::endl;
}

void filterAbstractPointerIsPresentTrueAndPredicateTrue(){
    class Test{ public: int const x = 10; };
    assert(Optional<Test *>::of(new Test()).filter([](Test const *const value) { return value->x == 10; }).get().x == 10);
    std::cout << "[TEST]::[filterAbstractPointerIsPresentTrueAndPredicateTrue]::SUCCESS" << std::endl;
}

void filterAbstractPointerIsPresentFalseAndPredicateFalse(){
    class Test{ public: int const x = 10; };
    assert(Optional<Test *>::ofNullable(nullptr).filter([](Test const *const value) { return value->x == 10; }).isEmpty());
    assert(Optional<Test *>::empty().filter([](Test const *const value) { return value->x == 10; }).isEmpty());
    std::cout << "[TEST]::[filterAbstractPointerIsPresentFalseAndPredicateFalse]::SUCCESS" << std::endl;
}

void filterAbstractPointerIsPresentTrueAndPredicateFalse(){
    class Test{ public: int const x = 10; };
    assert(Optional<Test *>::of(new Test()).filter([](Test const *const value) { return value->x == 20; }).isEmpty());
    std::cout << "[TEST]::[filterAbstractPointerIsPresentTrueAndPredicateFalse]::SUCCESS" << std::endl;
}