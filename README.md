# **Optional - C++** 

<img alt="Snowflask" src="optional.png" width="100">

Optional is a library that encapsulate a value and has operations on it. 
Optional works controlling null values, if the value is present you can get() it, else will throws an exception. 

## **Sumary**


- [About Me](#about-me)
- #### **Static methods**
- [Of](#of-method)
- [OfNullable](#ofnullable-method)
- [Empty](#empty-method)
- #### **Instance methods**
- [Get](#get-method)
- [GetPointer](#getpointer-method)
- [IsEmpty](#isempty-method)
- [IsPresent](#ispresent-method)
- [OrElse](#orelse-method)
- [OrElseGet](#orelseget-method)
- [OrElseThrow](#orelsethrow-method)
- [Filter](#filter-method)
- [Map](#map-method)
- [IfPresent](#ifpresent-method)
- [IfNotPresent](#ifnotpresent-method)
- [Execute](#execute-method)

---

## **`Optional static methods`** 

---

1. #### *of method*

```cpp
    static Optional<T> of( T const t );
    static Optional<T *> of( T const * const t );
```
- *`Optional::of`* is a method that encapsulate the input value. This method don't allow an input value null.

Input | Output
------|-------
T     | Optional< T >
T *   | Optional< T * >

---

2. #### *ofNullable method*

```cpp
    static Optional<T *> ofNullable( T const * const t );
    static Optional<T> ofNullable( T const t );
```

- *`Optional::ofNullable`* is a method that encapsulate the input value, but also allow null.

Input | Output
------|-------
T *   | Optional< T * >
T     | Optional< T >

---

3. #### *empty method*

```cpp
    static Optional<T *>empty();
```

- *`Optional::empty`* is a method that encapsulate null.

Input | Output
------|-------
\-    | Optional< T * >
\-    | Optional< T >

---

## **`Optional instance methods`** 

---

1. #### *get method*

```cpp
    T const get();
```
- *`Optional.get`* is a method that return the encapsulated value. If the value is primitive you can get the value, but if the value is pointer you only can get it if value is present, otherwise throw an exception(nullptr_value).

Input | Output
------|-------
\-    | T

---

2. #### *getPointer method*

```cpp
    T const * const getPointer();
```
- *`Optional.getPointer`* is a method that return the encapsulated pointer. If the pointer is present return it, otherwise throw an exception(nullptr_value).

Input | Output
------|-------
\-    | T *

---

3. #### *isEmpty method*

```cpp
    bool isEmpty();
```
- *`Optional.isEmpty`* is a method that checks if value is null. If value is null return true(empty) otherwise false(not present).

Input | Output
------|-------
\-    | bool

---

4. #### *isPresent method*

```cpp
    bool isPresent();
```
- *`Optional.isPresent`* is a method that checks if value is not null. If value is null return false(not present) otherwise true(present).

Input | Output
------|-------
\-    | bool

---

5. #### *orElse method*

```cpp
    T const orElse( T const other );
    T const * const orElse( T const * const other );
```
- *`Optional.orElse`* is a method that if encapsulated value is present return it, otherwise return the input value other. In the pointer other if it is null throws an exception(std::invalid_argument).

Input | Output
------|-------
T     | T
T *   | T *

---

6. #### *orElseGet method*

```cpp
    template <class S> using Provider = std::function< S () >;
```
- *`Provider`* is a lambda function that don't have input and return S.

```cpp
    T const orElseGet( Provider<T const> const provider );
    T const * const orElseGetPointer( Provider<T const * const> const provider );
```
- *`Optional.orElseGet`* is a method that if encapsulated value is present return it, otherwise return the execution of provider. Provider cannot be null, if it's, will throw an exception(std::invalid_argument).

Input         | Output
--------------|-------
Provider< T >   | T
Provider< T * > | T *

---

7. #### *orElseThrow method*

```cpp
    template <class E> T const getOrElseThrow( Provider<E const> const provider );
    template <class E> T const * const getPointerOrElseThrow( Provider<E const> const provider );
```
- *`Optional.orElseThrow`* is a method that if encapsulated value is present return it, otherwise throw an exception by provider. Provider cannot be null, if it's, will throw an exception(std::invalid_argument).

Input         | Output
--------------|-------
Provider< E > | T
Provider< E > | T *

---

8. #### *filter method*

```cpp
    template <class E> using Predicate = std::function< bool ( E ) >;
```
- *`Predicate`* is a lambda function that enter E and return boolean.

```cpp
    Optional<T *> filter( Predicate<T const * const> const predicate );
```
- *`Optional.filter`* is a method that checks if value is present and execute predicate in encapsulated value. If this is valid return a new optional of encapsulated value, otherwise return an empty optional. Predicate cannot be null, if it's, will throw an exception(std::invalid_argument).

Input            | Output
-----------------|------------------
Predicate< T * > | Optional< T * >

---

9. #### *map method*

```cpp
    template <class T, class S> using Mapper = std::function< S ( T ) >;
```
- *`Mapper`* is a lambda function that enter T and return S. Map T in S.

```cpp
    template <class E> Optional<E> map( Mapper<T const, E const> const mapper );
    template <class E> Optional<E> map( Mapper<T const * const, E const> const mapper );
```
- *`Optional.map`* is a method that checks if value is present and map a value from a type to another. If encapsulated value is present return a new optional with mapped value, otherwise return an empty optional. Mapper cannot be null, if it's, will throw an exception(std::invalid_argument).

Input            | Output
-----------------|------------------
Mapper< T, E >   | Optional< E >
Mapper< T *, E > | Optional< E >

---

10. #### *ifPresent method*

```cpp
    template <class R> using Consumer = std::function< void ( R ) >;
```
- *`Consumer`* is a lambda function that enter R and return nothing.

```cpp
    void ifPresent( Consumer<T const * const> const consumer );
```
- *`Optional.ifPresent`* is a method that checks if value is present, if it is consume sumething, otherwise do nothing. Consumer cannot be null, if it's, will throw an exception(std::invalid_argument).

Input            | Output
-----------------|------------------
Consumer< T * >  | void

---

11. #### *ifNotPresent method*

```cpp
    using Action = std::function< void () >;
```
- *`Action`* is a lambda function that don't have input and return nothing.

```cpp
    void ifNotPresent( Action const action );
```
- *`Optional.ifNotPresent`* is a method that checks if value is not present, if it is not execute an action, otherwise do nothing. Action cannot be null, if it's, will throw an exception(std::invalid_argument).

Input            | Output
-----------------|------------------
Action           | void

---

12. #### *execute method*

```cpp
    void execute( Consumer<T const> const consumer );
```
- *`Optional.execute`* is a method that consume something, execute action in encapsulated value. Consumer cannot be null, if it's, will throw an exception(std::invalid_argument).

Input            | Output
-----------------|------------------
Consumer< T >    | void

---

### **`About Me`**

> **Nick** : *STNIN*

> **Author** : *Thiago Hayaci*

> **Email** : *tyhayaci@gmail.com*

> **gitlab** : *https://gitlab.com/STNIN*

> **licence** : *free*
