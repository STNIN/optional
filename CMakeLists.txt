cmake_minimum_required(VERSION 3.18)

project( winter_optional )

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

enable_testing()

include( CTest )

set( SOURCE_FILES_TEST_OPTIONAL 
	include/lambda/lambda.hpp
	include/optional/exception/nullptr-exception.hpp
	include/optional/optional.hpp
	include/optional/optional-pointer.hpp
	test/optional/of-test.hpp
	test/optional/ofNullable-test.hpp
	test/optional/get-test.hpp
	test/optional/isEmpty-test.hpp
	test/optional/isPresent-test.hpp
	test/optional/orElse-test.hpp
	test/optional/orElseGet-test.hpp
	test/optional/orElseThrow-test.hpp
	test/optional/filter-test.hpp
	test/optional/map-test.hpp
	test/optional/ifPresent-test.hpp
	test/optional/ifNotPresent-test.hpp
	test/optional/execute-test.hpp
	test/optional/main-test.cpp
)
add_executable( winter_test_optional ${SOURCE_FILES_TEST_OPTIONAL} )
add_test( winter_optional_test winter_test_optional )
